﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace colores
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        void OnSliderValueChanged(object sender,ValueChangedEventArgs args)
        {
            /* if(sender == id_sl_blue)
             { 
             DisplayAlert("Error", "Prueba de colores", "OK");
             }*/

            id_bv_backgroud.BackgroundColor = Color.FromRgba((int)id_sl_red.Value,(int)id_sl_green.Value,(int)id_sl_blue.Value,(int)id_sl_alpha.Value);

            if(sender == id_sl_red)
            {
                id_lb_red.Text = id_sl_red.Value.ToString();
            }
            else if (sender == id_sl_green)
            {
                id_lb_green.Text = id_sl_green.Value.ToString();
            }
            else if (sender == id_sl_blue)
            {
                id_lb_blue.Text = id_sl_blue.Value.ToString();
            }
            else
            {
                id_lb_alpha.Text = id_sl_alpha.Value.ToString();
            }
        }
	}
}
